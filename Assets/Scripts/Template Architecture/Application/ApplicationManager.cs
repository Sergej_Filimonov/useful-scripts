﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.Architecture.Templates
{
    public class ApplicationManager : MonoBehaviour
    {
        [SerializeField] private AudioListener audioListener = null;
        [SerializeField] private EventSystem eventSystem = null;

        public static ApplicationManager Instance { get; private set; }

        public AudioListener AudioListener => audioListener;
        public EventSystem EventSystem => eventSystem;
        public LoadingScreen LoadingScreen { get; private set; }
        public UICamera UICamera { get; private set; }
        public AudioSystem AudioSystem { get; private set; }
        public MainMenu MainMenu { get; private set; }
        public Game Game { get; private set; }
        public GameField GameField { get; private set; }

        private void Awake()
        {
            Instance = this;
        }

        public void SetData(LoadingScreen loadingScreen, UICamera uICamera)
        {
            LoadingScreen = loadingScreen;
            UICamera = uICamera;
            LoadOtherScenes();
        }

        private void LoadOtherScenes()
        {
            AudioSystem = new AudioSystem();
            AudioSystem.Load(false, true, LoadingScreen);
            MainMenu = new MainMenu();
            MainMenu.Load(loadingScreenHolder: LoadingScreen);
        }
    }
}