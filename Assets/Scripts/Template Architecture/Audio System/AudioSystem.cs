﻿namespace Common.Architecture.Templates
{
    public class AudioSystem : SceneImplementer<AudioSystemController, AudioSystemSceneData, EmptyData>
    {
        protected override void SetScene()
        {
            Scene = SceneManagement.ApplicationScene.AudioSystem;
        }
    }
}