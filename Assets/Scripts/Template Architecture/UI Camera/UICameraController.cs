﻿namespace Common.Architecture.Templates
{
    public class UICameraController : SceneController<UICameraSceneData>
    {
        public UICameraSceneData SceneDataImplementation => sceneData;
    }
}