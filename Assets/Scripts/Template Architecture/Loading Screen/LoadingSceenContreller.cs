﻿namespace Common.Architecture.Templates
{
    public class LoadingSceenContreller : SceneController<LoadingScreenSceneData>
    {
        public LoadingScreenSceneData SceneDataImplementation => sceneData;
    }
}