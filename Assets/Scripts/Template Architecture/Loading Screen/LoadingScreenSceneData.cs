﻿using UnityEngine;

namespace Common.Architecture.Templates
{
    public class LoadingScreenSceneData : MonoBehaviour
    {
        [SerializeField] private SceneManagement.LoadingScreen loadingScreen = null;

        public SceneManagement.LoadingScreen LoadingScreen => loadingScreen;
    }
}