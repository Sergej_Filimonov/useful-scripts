﻿using Common.SceneManagement;
using UnityEngine;

namespace Common.Architecture.Templates
{
    public class LoadingScreen : SceneImplementer<LoadingSceenContreller, LoadingScreenSceneData, EmptyData>, ILoadingScreenHolder
    {
        protected override void SetScene()
        {
            Scene = SceneManagement.ApplicationScene.LoadingScreen;
        }

        public void ShowLoadingScreen(AsyncOperation asyncOperation)
        {
            Controller.SceneDataImplementation.LoadingScreen.Show(asyncOperation);
        }

        public void HideLoadingScreen()
        {
            Controller.SceneDataImplementation.LoadingScreen.Hide();
        }
    }
}