﻿using Common.Architecture;
using Common.Architecture.Templates;

public class MainMenu : SceneImplementer<MainMenuController, MainMenuSceneData, EmptyData>
{
    protected override void SetScene()
    {
        Scene = Common.SceneManagement.ApplicationScene.MainMenu;
    }
}
