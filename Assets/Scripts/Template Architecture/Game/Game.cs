﻿namespace Common.Architecture.Templates
{
    public class Game : SceneImplementer<GameController, GameSceneData, GameData>
    {
        protected override void SetScene()
        {
            Scene = SceneManagement.ApplicationScene.Game;
        }
    }
}