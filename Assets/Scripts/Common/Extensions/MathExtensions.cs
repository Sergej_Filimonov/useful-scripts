﻿using System;

namespace Common.Extensions
{
    public static class MathExtensions
    {
        public static bool HasSameSign(this float number1, float number2)
        {
            return Math.Sign(number1) == Math.Sign(number2);
        }
    }
}
