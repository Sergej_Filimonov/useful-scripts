﻿using UnityEngine;

public static class RectTransformExtensions
{
    public static void SetDefaultScale(this RectTransform trans)
    {
        trans.localScale = new Vector3(1, 1, 1);
    }

    public static void SetPivotAndAnchors(this RectTransform trans, Vector2 vec)
    {
        trans.pivot = vec;
        trans.anchorMin = vec;
        trans.anchorMax = vec;
    }

    public static void SetPivotAndAnchors(this RectTransform trans, Vector2 pivot, Vector2 anchorMin, Vector2 anchorMax)
    {
        trans.pivot = pivot;
        trans.anchorMin = anchorMin;
        trans.anchorMax = anchorMax;
    }

    public static Vector2 GetSize(this RectTransform trans)
    {
        return trans.rect.size;
    }

    public static float GetWidth(this RectTransform trans)
    {
        return trans.rect.width;
    }

    public static float GetHeight(this RectTransform trans)
    {
        return trans.rect.height;
    }

    public static float GetScaledWidth(this RectTransform trans)
    {
        return trans.rect.width * trans.localScale.x;
    }

    public static float GetScaledHeight(this RectTransform trans)
    {
        return trans.rect.height * trans.localScale.y;
    }

    public static Vector2 GetScaledSize(this RectTransform trans)
    {
        return new Vector2(trans.GetScaledWidth(), trans.GetScaledHeight());
    }

    public static void SetPositionOfPivot(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x, newPos.y, trans.localPosition.z);
    }

    public static void SetLeftBottomPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width),
            newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
    }

    public static void SetLeftTopPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width),
            newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
    }

    public static void SetRightBottomPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width),
            newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
    }

    public static void SetRightTopPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width),
            newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
    }

    public static void SetSize(this RectTransform trans, Vector2 newSize)
    {
        Vector2 oldSize = trans.rect.size;
        Vector2 deltaSize = newSize - oldSize;
        trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
        trans.offsetMax = trans.offsetMax +
                            new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
    }

    public static void SetWidth(this RectTransform trans, float newSize)
    {
        SetSize(trans, new Vector2(newSize, trans.rect.size.y));
    }

    public static void SetHeight(this RectTransform trans, float newSize)
    {
        SetSize(trans, new Vector2(trans.rect.size.x, newSize));
    }

    public static Rect GetWorldRect(this RectTransform trans, Vector2 scale)
    {
        Vector3[] corners = new Vector3[4];
        trans.GetWorldCorners(corners);
        Vector3 topLeft = corners[0];

        Vector2 scaledSize = new Vector2(scale.x * trans.rect.size.x, scale.y * trans.rect.size.y);

        return new Rect(topLeft, scaledSize);
    }
}