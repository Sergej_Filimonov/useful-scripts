﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Extensions
{
    public static class CollectionExtensions
    {
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.Shuffle(new Random());
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rng)
        {
            return source.ShuffleIterator(rng);
        }

        private static IEnumerable<T> ShuffleIterator<T>(this IEnumerable<T> source, Random rng)
        {
            var buffer = source.ToList();
            for (int i = 0; i < buffer.Count; i++)
            {
                int j = rng.Next(i, buffer.Count);
                yield return buffer[j];
                buffer[j] = buffer[i];
            }
        }

        public static T[] ToArray<T>(this Array arr)
        {
            return (T[]) arr;
        }

        public static T Random<T>(this IEnumerable<T> collection)
        {
            int count = UnityEngine.Random.Range(0, collection.Count());
            return collection.ElementAtOrDefault(count);
        }

        public static T NextOf<T>(this IEnumerable<T> collection, T item)
        {
            return collection.ElementAtOrDefault(collection.IndexOf(item) + 1 == collection.Count() ? 0 : (collection.IndexOf(item) + 1));
        }

        public static T PrevOf<T>(this IEnumerable<T> collection, T item)
        {
            return collection.ElementAtOrDefault(collection.IndexOf(item) - 1 < 0 ? collection.Count() - 1 : (collection.IndexOf(item) - 1));
        }

        public static T Random<T>(this IEnumerable<T> collection, Random random)
        {
            int count = random.Next(0, collection.Count());
            return collection.ElementAtOrDefault(count);
        }

        public static T Random<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc)
        {
            return Random(collection, sumGetterFunc, () => UnityEngine.Random.Range(0f, 1f));
        }

        public static T Random<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc, Random rnd)
        {
            return Random(collection, sumGetterFunc, () => (float) rnd.NextDouble());
        }

        private static T Random<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc, Func<float> rnd01Func)
        {

            float r = rnd01Func()*Sum(collection, element => sumGetterFunc(element));
            float sum = 0;
            foreach (var element in collection)
            {
                var value = sumGetterFunc(element);
                if (r <= sum + value)
                    return element;
                sum += value;
            }
            return default(T);
        }

        public static float Sum<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc)
        {
            float sum = 0;
            foreach (var element in collection)
            {
                sum += sumGetterFunc(element);
            }
            return sum;
        }

        public static int Max<T>(this IEnumerable<T> collection, Func<T, int> maxGetterFunc)
        {
            int max = int.MinValue;
            foreach (var element in collection)
            {
                var value = maxGetterFunc(element);
                if (value > max)
                    max = value;
            }
            return max;
        }

        public static int IndexOf<T>(this IEnumerable<T> collection, T element)
        {
            return collection.ToList().IndexOf(element);
        }
        
        public static void ForEach<T>(this IList<T> source, Action<int, T> action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            for (int index = 0; index < source.Count; ++index)
                action(index, source[index]);
        }
		
		public static void ForEach<T>(this T[] source, Action<T> action)
		{
			if (action == null) throw new ArgumentNullException(nameof(action));
			for (int index = 0; index < source.Length; ++index)
				action(source[index]);
		}

        public static string ToStringWithSeparator<T>(this IEnumerable<T> collection, char separator = ' ')
        {
	        if (collection == null || collection.Count() < 0) return string.Empty;

			var stringBuilder = new StringBuilder();
            T[] array = collection.ToArray();

            for (int i = 0; i < array.Length; i++)
			{
				stringBuilder.Append(array[i]);

                if (i < array.Length - 1)
                {
					stringBuilder.Append(separator);
				}
            }

            return stringBuilder.ToString();
        }

        public static bool IsContentEqualTo(this IEnumerable<string> collection, IEnumerable<string> otherCollection)
        {
            bool isContentEqual = true;

            foreach (var collItem in collection)
            {
                bool hasItem = false;

                foreach (var otherCollItem in otherCollection)
                {
                    if (string.Equals(collItem, otherCollItem, StringComparison.OrdinalIgnoreCase))
                    {
                        hasItem = true;
                        break;
                    }
                }

                if (!hasItem)
                {
                    isContentEqual = false;
                    break;
                }
            }

            return isContentEqual;
        }
    }
}
