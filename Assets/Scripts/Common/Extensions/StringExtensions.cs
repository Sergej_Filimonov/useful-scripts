﻿using System;
using System.Linq;
using UnityEngine;

public static class StringExtensions
{
	public static string ToTimeString(this TimeSpan timeSpan, string prefix = "")
	{
		string text = string.Empty;
		bool hasHours = timeSpan.Hours > 0;
		bool hasMinutes = timeSpan.Minutes > 0;

		if (hasHours)
		{
			text = string.Format("{0} ({1}h {2}m)", prefix, timeSpan.Hours, timeSpan.Minutes);
		}
		else if (hasMinutes)
		{
			text = string.Format("{0} ({1}m {2}s)", prefix, timeSpan.Minutes, timeSpan.Seconds);
		}
		else
		{
			text = string.Format("{0} ({1}s)", prefix, timeSpan.Seconds);
		}

		return text;
	}

    public static string GetLeaderboardPositionString(this int order)
    {
        string orderString = order.ToString();
        string suffix;

        if (orderString.Last() == '1')
        {
            suffix = "st";
        }
        else
        {
            switch (order)
            {
                case 2:
                    suffix = "nd";
                    break;
                case 3:
                    suffix = "rd";
                    break;
                default:
                    suffix = "th";
                    break;
            }
        }

        return order + suffix;
    }

    public static int[] ToIntArray(this string str, char separator = ' ')
    {
        if (string.IsNullOrEmpty(str)) return new int[]{};
        string[] strings = str.Split(new char[]{ separator }, StringSplitOptions.RemoveEmptyEntries);
        int[] numbers = new int[strings.Length];

        for (int i = 0; i < strings.Length; i++)
        {
            numbers[i] = int.Parse(strings[i]);
        }

        return numbers;
    }

    public static bool ToBool(this string str)
    {
        if (String.Equals(str, "True", StringComparison.OrdinalIgnoreCase))
        {
            return true;
        }
        else if (String.Equals(str, "False", StringComparison.OrdinalIgnoreCase))
        {
            return false;
        }

        Debug.LogErrorFormat("String {0} can not be converted to a bool value!", str);
        return false;
    }

	public static string PadZero(this int number) => number > 9 ? number.ToString() : "0" + number;
}
