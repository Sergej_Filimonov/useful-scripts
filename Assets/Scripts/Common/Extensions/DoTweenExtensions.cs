﻿using DG.Tweening;
using TMPro;

public static class DoTweenExtensions
{
    public static Tween PlayTextAnimation(this TMP_Text text, int goal, float time, string appendix = "")
    {
        float value = 0f;
        return DOTween.To(() => value, x => value = x, goal, time)
            .OnUpdate(() => text.text = ((int)value).ToString())
            .OnComplete(() => text.text = ((int)value).ToString());
    }
}
