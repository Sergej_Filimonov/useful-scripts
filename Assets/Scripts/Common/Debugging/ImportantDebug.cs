﻿using UnityEngine;

namespace UnityLibrary.Debugging
{
  public static class ImportantDebug
  {
    public static void Log(string message)
    {
      Debug.Log(string.Format("<color=green>{0}</color>", message));
    }

    public static void LogWarning(string message)
    {
      Debug.LogWarning(string.Format("<color=yellow>{0}</color>", message));
    }

    public static void LogError(string message)
    {
      Debug.LogError(string.Format("<color=red>{0}</color>", message));
    }
  }
}