using System;
using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UnityLibrary.Debugging
{
    /// <summary>
    /// Calculates frames per second and displays it.
    /// Debug class.
    /// </summary>
    public sealed class FpsCounter : MonoBehaviour
    {
        #region FIELDS
        [Header("System")]
        [SerializeField] private bool ignoreDebugMode = false;
        [SerializeField] private CalculateMethod calculateMethod = CalculateMethod.Mixed;
        [SerializeField] private float updateInterval = 0.5f;
        [Space]
        [Header("Alter text view")]
        [SerializeField] private Text outputText = null;
        [Space]
        [Header("Text view")]
        [SerializeField] private Padding padding = new Padding(0, 0, 0, 0);
        [SerializeField] private CounterPosition position = CounterPosition.UpperLeft;
        [SerializeField] private bool bold = true;
        [SerializeField] private int fontSize = 50;
        [SerializeField] private Color fontColor = Color.yellow;

        private float frames = 0;
        private float timeLeft = 0.0f;
        private int currentFps = 0;

        private WaitForSeconds waitFor;
        private float lastTime = 0;
        private int lastFrame = 0;

        private float deltaTime = 0.0f;

        private float fontScale = 2.0f;
        private GUIStyle style = new GUIStyle();
        private Rect rect;

        private static string[] stringsFrom00To300 = {
            "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
            "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
            "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
            "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
            "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
            "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
            "60", "61", "62", "63", "64", "65", "66", "67", "68", "69",
            "70", "71", "72", "73", "74", "75", "76", "77", "78", "79",
            "80", "81", "82", "83", "84", "85", "86", "87", "88", "89",
            "90", "91", "92", "93", "94", "95", "96", "97", "98", "99",
            "100", "101", "102", "103", "104", "105", "106", "107", "108", "109",
            "110", "111", "112", "113", "114", "115", "116", "117", "118", "119",
            "120", "121", "122", "123", "124", "125", "126", "127", "128", "129",
            "130", "131", "132", "133", "134", "135", "136", "137", "138", "139",
            "140", "141", "142", "143", "144", "145", "146", "147", "148", "149",
            "150", "151", "152", "153", "154", "155", "156", "157", "158", "159",
            "160", "161", "162", "163", "164", "165", "166", "167", "168", "169",
            "170", "171", "172", "173", "174", "175", "176", "177", "178", "179",
            "180", "181", "182", "183", "184", "185", "186", "187", "188", "189",
            "190", "191", "192", "193", "194", "195", "196", "197", "198", "199",
            "200", "201", "202", "203", "204", "205", "206", "207", "208", "209",
            "210", "211", "212", "213", "214", "215", "216", "217", "218", "219",
            "220", "221", "222", "223", "224", "225", "226", "227", "228", "229",
            "230", "231", "232", "233", "234", "235", "236", "237", "238", "239",
            "240", "241", "242", "243", "244", "245", "246", "247", "248", "249",
            "250", "251", "252", "253", "254", "255", "256", "257", "258", "259",
            "260", "261", "262", "263", "264", "265", "266", "267", "268", "269",
            "270", "271", "272", "273", "274", "275", "276", "277", "278", "279",
            "280", "281", "282", "283", "284", "285", "286", "287", "288", "289",
            "290", "291", "292", "293", "294", "295", "296", "297", "298", "299",
            "300"
        };
        #endregion FIELDS

        #region UNITY EVENTS

        private void Awake()
        {
            if (Debug.isDebugBuild)
            {
                enabled = true;
            }
            else
            {
                if (ignoreDebugMode && outputText != null)
                {
                    enabled = true;
                    outputText.transform.parent.gameObject.SetActive(true);
                }
                else
                {
                    enabled = false;
                }
            }
        }

        private void Start()
        {
            Debug.Log("FPS Counter is active!");

            if (Debug.isDebugBuild)
            {
                switch (calculateMethod)
                {
                    case CalculateMethod.UpdateOnly:
                        fontScale += 6;
                        break;
                }

                switch (position)
                {
                    case CounterPosition.UpperLeft:
                        rect = new Rect(0 + padding.Left, 0 + padding.Top, 0, 0);
                        break;
                    case CounterPosition.UpperRight:
                        rect = new Rect(Screen.width - (fontScale * fontSize) - padding.Right, 0 + padding.Top, 0, 0);
                        break;
                    case CounterPosition.BottomLeft:
                        rect = new Rect(0 + padding.Left, Screen.height - fontSize - padding.Bottom, 0, 0);
                        break;
                    case CounterPosition.BottomRight:
                        rect = new Rect(Screen.width - (fontScale * fontSize) - padding.Right, Screen.height - fontSize - padding.Bottom, 0, 0);
                        break;
                }

                style.fontSize = fontSize;
                style.normal.textColor = fontColor;
                if (bold) style.fontStyle = FontStyle.Bold;
                else style.fontStyle = FontStyle.Normal;

                switch (calculateMethod)
                {
                    case CalculateMethod.UpdateOnly:
                        Observable.EveryUpdate().Subscribe(_ =>
                        {
                            deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
                        }).AddTo(this);
                        break;
                    case CalculateMethod.Coroutine:
                        waitFor = new WaitForSeconds(updateInterval);
                        StartCoroutine(CountUpFPS());
                        break;
                    case CalculateMethod.Mixed:
                        Observable.EveryUpdate().Subscribe(_ =>
                        {
                            ++frames;
                            timeLeft += Time.deltaTime;
                        }).AddTo(this);

                        Observable.Timer(TimeSpan.FromSeconds(updateInterval)).Repeat().Subscribe(_ =>
                        {
                            currentFps = (int)Mathf.Clamp(frames / timeLeft, 0, 300);
                            frames = 0;
                            timeLeft = 0;
                        }).AddTo(this);
                        break;
                }
            }
            else
            {
                switch (calculateMethod)
                {
                    case CalculateMethod.UpdateOnly:
                        Observable.EveryUpdate().Subscribe(_ =>
                        {
                            float msec = deltaTime * 1000.0f;
                            float fps = 1.0f / deltaTime;
                            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
                            outputText.text = text;
                        }).AddTo(this);
                        break;
                    default:
                        StartCoroutine(DisplayFps());
                        break;
                }
            }
        }

        private void OnGUI()
        {
            switch (calculateMethod)
            {
                case CalculateMethod.UpdateOnly:
                    float msec = deltaTime * 1000.0f;
                    float fps = 1.0f / deltaTime;
                    string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
                    GUI.Label(rect, text, style);
                    break;
                default:
                    GUI.Label(rect, stringsFrom00To300[currentFps], style);
                    break;
            }
        }
        #endregion UNITY EVENTS

        private IEnumerator CountUpFPS()
        {
            while (true)
            {
                yield return waitFor;
                currentFps = (int)((Time.frameCount - lastFrame) / (Time.time - lastTime));
                lastFrame = Time.frameCount;
                lastTime = Time.time;
            }
        }

        private IEnumerator DisplayFps()
        {
            while (true)
            {
                yield return waitFor;
                outputText.text = stringsFrom00To300[currentFps];
            }
        }

        enum CounterPosition { UpperLeft, UpperRight, BottomLeft, BottomRight }
        enum CalculateMethod { UpdateOnly, Coroutine, Mixed }

        [Serializable]
        private struct Padding
        {
            public float Top;
            public float Bottom;
            public float Left;
            public float Right;

            public Padding(float top, float bot, float left, float right)
            {
                Top = top;
                Bottom = bot;
                Left = left;
                Right = right;
            }
        }
    }
}