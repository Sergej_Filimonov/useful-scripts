﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;

namespace Common.Effects
{
    public sealed class EffectHolder : MonoBehaviour
    {
        [SerializeField] private bool initOnAwake = true;
        [SerializeField] private int levelIndex = 0;
        [SerializeField] private List<ParticleSystem> particles = null;
        [SerializeField] private List<ParticleSystem> particlesToStop = null;
        [SerializeField] private List<PlayableDirector> timelines = null;

        public int LevelIndex { get { return levelIndex; } }

        private void Awake() { if (initOnAwake) Init(); }

        public void Init()
        {
            particles.Where(p => p != null).ToList().ForEach(p => p.gameObject.SetActive(false));
            timelines.Where(p => p != null).ToList().ForEach(t => t.gameObject.SetActive(false));
            // TODO: add Effect Manager
            //if (ContainerManager.Contains<ComboEffectsManager>())
            //    ContainerManager.Get<ComboEffectsManager>().RegisterEffectHolder(this);
        }

        public void Merge(EffectHolder other)
        {
            other.particles.ForEach(particles.Add);
            other.particlesToStop.ForEach(particlesToStop.Add);
            other.timelines.ForEach(timelines.Add);
            Destroy(other.gameObject); // TODO: may cause problems
        }

        public void Play()
        {
            if (particles.All(p => p != null)) particles.ForEach(p =>
            {
                if (!p.gameObject.activeSelf) p.gameObject.SetActive(true);
                p.Play();
                var particleSettings = p.main;
                particleSettings.playOnAwake = true;
            });
            if (particlesToStop.All(t => t != null)) particlesToStop.ForEach(p =>
            {
                p.Stop();
                var particleSettings = p.main;
                particleSettings.playOnAwake = false;
            });
            if (timelines.All(t => t != null)) timelines.ForEach(t =>
            {
                if (!t.gameObject.activeSelf) t.gameObject.SetActive(true);
                t.Play();
            });
            // TODO: add Sounds Player
            //EventHolder<SoundsPlayerEvents>.Dispatcher.Broadcast(SoundsPlayerEvents.SoundEffectPlay, "Combo_pop_up");
        }

        public void Stop()
        {
            if (particles.All(p => p != null)) particles.ForEach(p =>
            {
                p.Stop();
                var particleSettings = p.main;
                particleSettings.playOnAwake = false;
            });
            if (particlesToStop.All(t => t != null)) particlesToStop.ForEach(p =>
            {
                p.Stop();
                var particleSettings = p.main;
                particleSettings.playOnAwake = false;
            });
            if (timelines.All(t => t != null)) timelines.ForEach(t =>
            {
                t.Stop();
                if (t.gameObject.activeSelf) t.gameObject.SetActive(false);
            });
        }
    }
}