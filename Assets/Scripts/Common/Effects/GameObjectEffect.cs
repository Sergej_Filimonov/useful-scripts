﻿using UnityEngine;

namespace Common.Effects
{
    public sealed class GameObjectEffect : Effect
    {
        [SerializeField]
        private GameObject effect = null;

        public override float Duration
        {
            get { return 0.0f; }
        }

        public override void Play()
        {
            effect.SetActive(true);
        }

        public override void Stop()
        {
            effect.SetActive(false);
        }
    }
}
