﻿using UnityEngine;

namespace Common.Effects
{
    [RequireComponent(typeof(AudioSource))]
    public sealed class SoundEffect : Effect
    {
        [SerializeField] private AudioSource audioSource = null;

        public override float Duration
        {
            get { return audioSource.clip.length; }
        }

        public override void Play()
        {
            audioSource.Play();
        }

        public override void Stop()
        {
            audioSource.Stop();
        }
    }
}
