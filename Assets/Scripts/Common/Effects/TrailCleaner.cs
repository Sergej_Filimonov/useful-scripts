﻿using UnityEngine;

namespace Common.Effects
{
    [RequireComponent(typeof(TrailRenderer))]
    public sealed class TrailCleaner : MonoBehaviour
    {
        [SerializeField] private TrailRenderer trailRenderer = null;

        private void OnEnable()
        {
            trailRenderer.Clear();
        }
    }
}
