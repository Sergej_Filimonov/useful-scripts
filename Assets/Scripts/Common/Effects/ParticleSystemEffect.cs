﻿using UnityEngine;

namespace Common.Effects
{
    [RequireComponent(typeof(ParticleSystem))]
    public sealed class ParticleSystemEffect : Effect
    {
        [SerializeField] private ParticleSystem effect = null;

        public override float Duration
        {
            get { return effect.main.duration; }
        }

        public override void Play()
        {
            effect.Play();
        }

        public override void Stop()
        {
            effect.Stop();
        }
    }
}
