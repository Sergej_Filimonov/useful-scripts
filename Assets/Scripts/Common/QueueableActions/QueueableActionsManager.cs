﻿using System.Collections.Generic;

namespace Common.Queueable
{
    public sealed class QueueableActionsManager
    {
        private Queue<QueueableAction> queueableActions;
        private QueueableAction currentAction;
        private Dictionary<int, Queue<QueueableAction>> queues;
        private Dictionary<int, QueueableAction> executingActions;

        public QueueableActionsManager()
        {
            queueableActions = new Queue<QueueableAction>();
            queues = new Dictionary<int, Queue<QueueableAction>>();
            executingActions = new Dictionary<int, QueueableAction>();
        }

        public void Add(QueueableAction queueableAction)
        {
            queueableActions.Enqueue(queueableAction);

            bool needRun = true;

            if (currentAction != null && currentAction.isRunning)
            {
                needRun = false;
            }

            queueableAction.Completed += RunNext;
            if (needRun) RunNext();
        }

        public void Add(QueueableAction queueableAction, int indexQueue)
        {
            if (!queues.ContainsKey(indexQueue)) queues.Add(indexQueue, new Queue<QueueableAction>());

            queues[indexQueue].Enqueue(queueableAction);

            bool needRun = true;

            if (executingActions.ContainsKey(indexQueue))
            {
                var action = executingActions[indexQueue];

                if (action != null && action.isRunning)
                {
                    needRun = false;
                }
            }

            queueableAction.Completed += () => RunNext(indexQueue);
            if (needRun) RunNext(queueableAction, indexQueue);
        }

        public void Reset()
        {
            queueableActions.Clear();
            currentAction = null;
        }

        private void RunNext()
        {
            if (queueableActions.Count > 0)
            {
                currentAction = queueableActions.Dequeue();
                currentAction.Run();
            }
        }

        private void RunNext(QueueableAction action, int indexQueue)
        {
            executingActions.Remove(indexQueue);
            executingActions.Add(indexQueue, action);
            queues[indexQueue].Dequeue();
            action.Run();
        }

        private void RunNext(int indexQueue)
        {
            if (queues[indexQueue].Count > 0)
            {
                var action = queues[indexQueue].Peek();
                RunNext(action, indexQueue);
            }
            else
            {
                executingActions.Remove(indexQueue);
            }
        }
    }
}