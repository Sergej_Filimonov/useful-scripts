﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.EnvironmentSetup
{
    // TODO: add interface
    public sealed class EnvironmentManager// : IManager
    {
        private Dictionary<int, GameObject> items = new Dictionary<int, GameObject>();

        #region IManager

        public void Start()
        {

        }

        public void Stop()
        {

        }

        #endregion

        public void AddItems(List<CustomItem> items, Transform itemsParent)
        {
            if (itemsParent == null)
            {
                Debug.LogError("Items parent not found!");
                return;
            }

            if (items == null)
            {
                Debug.LogError("Custom items not found!");
                return;
            }

            items.ForEach(i => AddItem(i, itemsParent));
        }

        public void AddItem(CustomItem item, Transform itemsParent)
        {
            if (itemsParent == null)
            {
                Debug.LogError("Items parent not found!");
                return;
            }

            if (item == null || item.Prefab == null)
            {
                Debug.LogError("Custom item not found!");
                return;
            }

            if (items.ContainsKey(item.ID))
            {
                Debug.LogError($"Item with {item.ID} ID already exist!");
                return;
            }

            if (items.ContainsKey(item.ReplacementID))
            {
                var replacementID = item.ReplacementID;

                Object.Destroy(items[replacementID]);
                items.Remove(replacementID);
            }

            var newItem = Object.Instantiate(item.Prefab, itemsParent);

            items.Add(item.ID, newItem);
        }

        public void RemoveItems()
        {
            items.Values.ToList().ForEach(i => Object.Destroy(i));
            items.Clear();
        }
    }
}