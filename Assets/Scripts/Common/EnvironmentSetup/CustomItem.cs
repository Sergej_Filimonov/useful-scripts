﻿using System;
using UnityEngine;

namespace Common.EnvironmentSetup
{
    [Serializable]
    public sealed class CustomItem
    {
        [SerializeField] private string name = "";
        [SerializeField] private int id = 0;
        [SerializeField] private int replacementId = -100;
        [SerializeField] private GameObject prefab = null;

        public string Name => name;
        public int ID => id;
        public int ReplacementID => replacementId;
        public GameObject Prefab => prefab;
    }
}