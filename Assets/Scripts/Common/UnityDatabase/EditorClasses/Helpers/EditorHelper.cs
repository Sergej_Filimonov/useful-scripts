﻿#if UNITY_EDITOR
using UnityEngine;

namespace Ejaw.UnityDatabase.EditorClasses.Helpers
{
    public static class EditorHelper
    {
        public static bool Button(string buttonName, Color buttonColor, bool buttonEnabled,
            params GUILayoutOption[] options)
        {
            UnityEngine.GUI.backgroundColor = buttonColor;
            UnityEngine.GUI.enabled = buttonEnabled;
            bool button = GUILayout.Button(buttonName, options);
            UnityEngine.GUI.enabled = true;
            UnityEngine.GUI.backgroundColor = Color.white;

            return button;
        }

        /// <summary>
        /// Create GUI button with name,
        /// color and make it enable or
        /// disable
        /// </summary>
        public static bool Button(string buttonName, Color buttonColor, bool buttonEnabled = true)
        {
            UnityEngine.GUI.backgroundColor = buttonColor;
            UnityEngine.GUI.enabled = buttonEnabled;
            bool button = GUILayout.Button(buttonName);
            UnityEngine.GUI.enabled = true;
            UnityEngine.GUI.backgroundColor = Color.white;

            return button;
        }
    }
}

#endif
