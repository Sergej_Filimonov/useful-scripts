﻿#if UNITY_EDITOR
using Ejaw.UnityDatabase.Core;
using Ejaw.UnityDatabase.Core.Records;
using Ejaw.UnityDatabase.Core.Tables;
using UnityEditor;

namespace Ejaw.UnityDatabase.EditorClasses.Windows
{
    public sealed class TablesDatabaseWindow : BaseTableWindow<TablesDatabase, TableRecord>
    {
        protected override void OnFocus()
        {
            if (table == null)
                table = UDB.GetTablesDatabase();
        }

        protected override void DispayTableData()
        {
            //Nothing to display
        }

        protected override void SelectTable()
        {
            //Nothing to select
        }

        [MenuItem("Tools/EJaw/Database/Tables database")]
        public static void OpenTablesDatabaseWindow()
        {
            GetWindow<TablesDatabaseWindow>(false, "Tables database", true);
        }
    }
}

#endif
