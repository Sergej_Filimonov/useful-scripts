﻿#if UNITY_EDITOR
using Ejaw.UnityDatabase.Core.Records;
using Ejaw.UnityDatabase.Core.Tables;

namespace Ejaw.UnityDatabase.EditorClasses.Layouts
{
    public interface IWindowLayout
    {
        /// <summary>
        /// Drawing tables based on aligment
        /// </summary>
        void DrawTable<TRecord>(IEditorTable<TRecord> table) where TRecord : IRecord, IEditorRecord;

        /// <summary>
        /// Drawing records based on aligment
        /// </summary>
        void DrawRecord<TTable, TRecord>(TTable table)
            where TTable : ScriptableObjectTable, ITable<TRecord>, IEditorTable<TRecord>
            where TRecord : IRecord, IEditorRecord, new();
    }
}

#endif
