﻿using System.IO;
using Ejaw.UnityDatabase.Core.Tables;
using UnityEditor;
using UnityEngine;

namespace Ejaw.UnityDatabase.Core
{
#if UNITY_EDITOR
#endif

    /// <summary>
    /// Main class for unity database
    /// </summary>
    public static class UDB
    {
        #region Runtime database

        private static IRuntimeUnityDatabase runtimeUnityDatabase = null;

        public static IRuntimeUnityDatabase RuntimeInstance
        {
            get
            {
                if (runtimeUnityDatabase == null)
                {
                    var td = LoadTablesDatabase();
#if UNITY_EDITOR
                    if (td == null)
                        td = CreateTablesDatabase();
#endif
                    runtimeUnityDatabase = new RuntimeUnityDatabase(td);
                }

                return runtimeUnityDatabase;
            }
        }

        #endregion

        #region Functions

        private static TablesDatabase LoadTablesDatabase()
        {
            return Resources.Load("TablesDatabase") as TablesDatabase;
        }


#if UNITY_EDITOR

        private static TablesDatabase tablesDatabase = null;

        public static TablesDatabase GetTablesDatabase()
        {
            if (tablesDatabase == null)
                tablesDatabase = CreateTablesDatabase();

            return tablesDatabase;
        }

        private static TablesDatabase CreateTablesDatabase()
        {
            var td = LoadTablesDatabase();
            if (td != null)
                return td;

            if (!Directory.Exists("Assets/Resources"))
                AssetDatabase.CreateFolder("Assets", "Resources");
            td = EditorClasses.Helpers.ScriptableObjectHelper.Create<TablesDatabase>("Assets/Resources/TablesDatabase.asset");

            if (td != null)
                Debug.Log("UnityDatabase: tables database created!");
            else
                Debug.LogError("UnityDatabase: failed create tables database!");

            return td;
        }

        public static TTable Create<TTable>(string tableName) where TTable : ScriptableObjectTable
        {
            if (!Directory.Exists("Assets/InternalAssets/Tables"))
                AssetDatabase.CreateFolder("Assets", "Tables");

            return EditorClasses.Helpers.ScriptableObjectHelper.Create<TTable>("Assets/InternalAssets/Tables/" + tableName + ".asset");
        }

#endif

        #endregion
    }
}
