﻿using System;
using Common.Serialization;
using FullSerializer;
using UnityEngine;

namespace Ejaw.UnityDatabase.Core.Records
{
#if UNITY_EDITOR
#endif

    /// <summary>
    /// Base implementation of database record.
    /// You can inherit for creation you own records.
    /// </summary>
    [Serializable]
    public abstract class BaseRecord : IRecord
#if UNITY_EDITOR
        , Ejaw.UnityDatabase.EditorClasses.IEditorRecord
#endif
    {
        /// <summary>
        /// Unique identifier in table 
        /// </summary>
        [SerializeField, fsIgnore] private int id = IdGenerator.INVALID_ID;

        public int Id => id;

#if UNITY_EDITOR
		void Ejaw.UnityDatabase.EditorClasses.IEditorRecord.SetId(int id)
        {
            this.id = id;
        }
#endif

	    public virtual string GetCustomDataForPlayFab()
	    {
		    return JsonSerializer.Serialize(this);
	    }
	}
}

