﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;
//
//namespace Common.Container
//{
//    public static class ContainerManager
//    {
//        private static readonly Dictionary<Type, IContainerItem> containerItems = new Dictionary<Type, IContainerItem>();
//
//        public static void Add<T>(T service) where T : IContainerItem
//        {
//            var t = typeof(T);
//            if (containerItems.ContainsKey(t))
//            {
//                Debug.LogError(string.Format("Failed add new item (type={0}, obj={1}) to the container due to services (type={0}, obj={2}) already exist.", t, service, containerItems[t]));
//            }
//            else
//            {
//                containerItems.Add(t, service);
//            }
//        }
//
//        public static void AddOrReplace<T>(T service) where T : IContainerItem
//        {
//            var t = typeof(T);
//            if (containerItems.ContainsKey(t))
//            {
//                containerItems.Remove(t);
//            }
//            containerItems.Add(t, service);
//        }
//
//        public static T Get<T>() where T : class, IContainerItem
//        {
//            var t = typeof (T);
//            if (containerItems.ContainsKey(t))
//                return containerItems[t] as T;
//
//            Debug.LogError(string.Format("Container item {0} doesn't exist in the container.", t));
//            return default(T);
//        }
//
//        public static T Get<T>(T defaultService) where T : class, IContainerItem
//        {
//            var t = typeof (T);
//            if (containerItems.ContainsKey(t))
//                return containerItems[t] as T;
//
//            Debug.LogWarning(string.Format("Container item {0} doesn't exist in the container.", t));
//            return defaultService;
//        }
//
//        public static bool Contains(Type type)
//        {
//            return containerItems.ContainsKey(type);
//        }
//
//        public static bool Contains<T>() where T : IContainerItem
//        {
//            return containerItems.ContainsKey(typeof (T));
//        }
//
//        public static void Remove<T>() where T : IContainerItem
//        {
//            var t = typeof (T);
//            if (containerItems.ContainsKey(t))
//            {
//                containerItems.Remove(t);
//            }
//        }
//
//        public static void Clear()
//        {
//            containerItems.Values.ToList().ForEach(containerItem =>
//            {
//                if (containerItem != null)
//                    containerItem.Dispose();
//            });
//            containerItems.Clear();
//        }
//    }
//}