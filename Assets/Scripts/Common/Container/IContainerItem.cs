﻿using System;

namespace Common.Container
{
    public interface IContainerItem : IDisposable
    {
        Type ItemType { get; }
    }
}
