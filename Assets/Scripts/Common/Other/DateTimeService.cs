﻿using System;
using System.IO;
using System.Net;
using System.Net.Cache;

namespace Common
{
    public sealed class DateTimeService
    {
        private static string SERVER_URL = "https://showcase.linx.twenty57.net:8081/UnixTime/tounixtimestamp?datetime=now";

        public static long GetUnixTime()
        {
            long unixTime = 0;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SERVER_URL);
            request.Method = "GET";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
            request.ContentType = "application/x-www-form-urlencoded";
            request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    string html = stream.ReadToEnd();
                    unixTime = long.Parse(html.Split(':')[1].Trim('\"', '}'));
                }
            }

            return unixTime;
        }
    }

    public static class DateTimeExtensions
    {
        public static int TotalSeconds(this DateTime dateTimeToConvert)
        {
            int secsInAMin = 60;
            int secsInAnHour = 60 * secsInAMin;
            int secsInADay = 24 * secsInAnHour;
            double secsInAYear = (int)365.25 * secsInADay;

            int totalSeconds = (int)(dateTimeToConvert.Year * secsInAYear) +
                           (dateTimeToConvert.DayOfYear * secsInADay) +
                           (dateTimeToConvert.Hour * secsInAnHour) +
                           (dateTimeToConvert.Minute * secsInAMin) +
                           dateTimeToConvert.Second;

            return totalSeconds;
        }

        public static int ToSecondsFromUnixStartDate(this DateTime dateTime)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            DateTime dtNow = DateTime.Now;

            TimeSpan result = dtNow.Subtract(dt);
            int seconds = Convert.ToInt32(result.TotalSeconds);

            return seconds;
        }
    }
}