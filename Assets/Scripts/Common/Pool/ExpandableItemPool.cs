﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Pool
{
    public class ExpandableItemPool<T> : IPool<T>
        where T : Component, IPoolItem
    {
        public delegate T CreatorDelegate();
        private readonly CreatorDelegate itemCreator;
        private readonly Action<T> itemInitializer;
        private readonly Queue<T> container;

        public int Size { get { return container.Count; } }

        protected int InitializedItemsCount { get; set; }

        public ExpandableItemPool(CreatorDelegate itemCreator, Action<T> itemInitializer, int preCreation = 0)
        {
            this.itemCreator = itemCreator;
            this.itemInitializer = itemInitializer;
            container = new Queue<T>();

            if (preCreation <= 0) return;
            for (var i = 0; i < preCreation; i++) Create();
        }
        
        public void PutBack(T item)
        {
            item.gameObject.SetActive(false);
            container.Enqueue(item);
        }

        public T GetItem()
        {
            if (Size == 0) Create();
            if (Size <= 0) return null;
            var item = container.Dequeue();
            item.gameObject.SetActive(true);
            return item;
        }

        public T GetItem(Vector3 spawnPosition, Quaternion spawnRotation)
        {
            if (Size == 0) Create();
            if (Size <= 0) return null;
            var item = container.Dequeue();
            item.transform.position = spawnPosition;
            item.transform.rotation = spawnRotation;
            item.gameObject.SetActive(true);
            return item;
        }

        public IEnumerable<T> GetAllItems() => container;

        public void Reset()
        {
            container.Clear();
            InitializedItemsCount = 0;
        }

        protected virtual void Create()
        {
            var item = itemCreator();
            itemInitializer(item);
            item.gameObject.SetActive(false);
            container.Enqueue(item);
            InitializedItemsCount++;
        }
    }
}
