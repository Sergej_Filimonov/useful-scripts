﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Common.Pool
{
    public sealed class PoolHelper
    {
        public static IPool<T> CreatePool<T>(string prefabPath, Action<T> itemInitializer, Transform container,
            int preCreation = 10) where T : Component, IPoolItem
        {
            ExpandableItemPool<T>.CreatorDelegate creator = () =>
            {
                var poolItem = Object.Instantiate(Resources.Load<T>(prefabPath));
                poolItem.transform.SetParent(container);
                poolItem.transform.localPosition = Vector3.zero;
                poolItem.transform.localScale = Vector3.one;
                return poolItem;
            };
            return new ExpandableItemPool<T>(creator, itemInitializer, preCreation);
        }

        public static IPool<T> CreatePool<T>(T prefab, Action<T> itemInitializer, Transform container,
            int preCreation = 10) where T : Component, IPoolItem
        {
            ExpandableItemPool<T>.CreatorDelegate creator = () =>
            {
                var poolItem = Object.Instantiate(prefab);
                poolItem.transform.SetParent(container);
                poolItem.transform.localPosition = Vector3.zero;
                poolItem.transform.localScale = Vector3.one;
                return poolItem;
            };
            return new ExpandableItemPool<T>(creator, itemInitializer, preCreation);
        }

        public static IPool<T> CreatePool<T>(T prefab, Action<T> itemInitializer, int preCreation = 10)
         where T : Component, IPoolItem
        {
            ExpandableItemPool<T>.CreatorDelegate creator = () =>
            {
                var poolItem = Object.Instantiate(prefab);
                return poolItem;
            };
            return new ExpandableItemPool<T>(creator, itemInitializer, preCreation);
        }

        public static IPool<T> CreatePool<T>(ExpandableItemPool<T>.CreatorDelegate creator, Action<T> itemInitializer,
            int preCreation = 10) where T : Component, IPoolItem
        {
            return new ExpandableItemPool<T>(creator, itemInitializer, preCreation);
        }
    }
}