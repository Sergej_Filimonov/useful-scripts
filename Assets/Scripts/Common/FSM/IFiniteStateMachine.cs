﻿using System;

namespace Common.FSM
{
    public interface IFiniteStateMachine<in TStatesType, TStateExecutor> : IDisposable
        where TStatesType : struct, IComparable, IConvertible, IFormattable
        where TStateExecutor : IState
    {
        event Action<TStateExecutor> StateChanged;

        void AddListener(TStateExecutor stateExecutor, TStatesType state);

        bool CanFire(TStatesType state);

        void Fire(TStatesType state);
    }
}
