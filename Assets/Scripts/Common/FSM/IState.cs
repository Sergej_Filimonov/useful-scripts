﻿namespace Common.FSM
{
    public interface IState
    {
        void OnEnterState();

        void OnExitState();
    }
}
