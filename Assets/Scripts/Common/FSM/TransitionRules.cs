﻿using System;
using System.Collections.Generic;

namespace Common.FSM
{
    public sealed class TransitionRules<TStatesType>
        where TStatesType : struct, IComparable, IConvertible, IFormattable
    {
        private IEnumerable<FSMRule<TStatesType>> rules;

        public TransitionRules(IEnumerable<FSMRule<TStatesType>> rules)
        {
            this.rules = rules;
        }

        public IEnumerable<FSMRule<TStatesType>> Rules
        {
            get { return rules; }
        }
    }
}
