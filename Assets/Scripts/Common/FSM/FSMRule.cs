﻿using System;

namespace Common.FSM
{
    public struct FSMRule<TStatesType>
        where TStatesType : struct, IComparable, IConvertible, IFormattable
    {
        private readonly TStatesType from;
        private readonly TStatesType to;

        public TStatesType From
        {
            get { return from; }
        }

        public TStatesType To
        {
            get { return to; }
        }

        public FSMRule(TStatesType from, TStatesType to)
        {
            this.from = from;
            this.to = to;
        }
    }
}
