﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.FSM
{
    public sealed class BaseFSM<TStatesType, TStateExecutor> : IFiniteStateMachine<TStatesType, TStateExecutor> 
        where TStatesType : struct, IComparable, IConvertible, IFormattable
        where TStateExecutor : IState
    {
        public event Action<TStateExecutor> StateChanged = delegate { };
        private TStatesType currentState;
        private TStatesType lastState;
        private IDictionary<TStatesType, IList<TStateExecutor>> executors = new Dictionary<TStatesType, IList<TStateExecutor>>();
        private readonly TransitionRules<TStatesType> transitionRules;
        
        public BaseFSM(TransitionRules<TStatesType> transitionRules)
        {
            this.transitionRules = transitionRules;
        }

        public BaseFSM(TransitionRules<TStatesType> transitionRules, TStatesType startState) 
            : this(transitionRules)
        {
            currentState = startState;
        }

        public void AddListener(TStateExecutor stateExecutor, TStatesType state)
        {
            if (executors.ContainsKey(state))
            {
                executors[state].Add(stateExecutor);
            }
            else
            {
                executors.Add(state, new List<TStateExecutor> { stateExecutor });
            }
        }

        public bool CanFire(TStatesType state)
        {
            return transitionRules.Rules.Any(rule => rule.From.ToString() == currentState.ToString() &&
                        rule.To.ToString() == state.ToString());
        }

        public void Fire(TStatesType state)
        {
            if (CanFire(state))
            {
                lastState = currentState;
                currentState = state;

                IList<TStateExecutor> lastStateExtecutors;
                executors.TryGetValue(lastState, out lastStateExtecutors);
                if (lastStateExtecutors != null && lastStateExtecutors.Any(lastStateExtecutor => lastStateExtecutor != null))
                {
                    for (var i = 0; i < lastStateExtecutors.Count; i++)
                    {
                        lastStateExtecutors[i].OnExitState();
                    }
                }

                IList<TStateExecutor> currentStateExtecutors;
                executors.TryGetValue(currentState, out currentStateExtecutors);
                if (currentStateExtecutors != null && currentStateExtecutors.Any(currentStateExtecutor => currentStateExtecutor != null))
                {
                    #if UNITY_SWITCH && !UNITY_EDITOR
                    Debug.LogError(string.Format("FSM fire to state: {0} from {1} state", state, currentState));
                    #endif
                    for (var i = 0; i < currentStateExtecutors.Count; i++)
                    {
                        currentStateExtecutors[i].OnEnterState();
                        StateChanged(currentStateExtecutors[i]);
                    }
                }
            }
            else
            {
                Debug.LogError(string.Format("FSM can't fire to state: {0} from {1} state", state, currentState));
            }
        }

        void IDisposable.Dispose()
        {
            executors = new Dictionary<TStatesType, IList<TStateExecutor>>();
        }
    }
}
