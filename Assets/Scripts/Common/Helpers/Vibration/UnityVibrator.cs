using UnityEngine;

namespace Helpers.Vibration
{
	public sealed class UnityVibrator : IVibrator
	{
		public void Vibrate() => Handheld.Vibrate();
		public void Vibrate(long milliseconds) => Handheld.Vibrate();
		public void Vibrate(long[] pattern, int repeat) => Handheld.Vibrate();
		public void Cancel() {}
	}
}