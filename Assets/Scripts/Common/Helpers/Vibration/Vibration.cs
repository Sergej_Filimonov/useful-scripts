namespace Helpers.Vibration
{
	public static class Vibration
	{
		private static readonly IVibrator Vibrator =
#if UNITY_ANDROID && !UNITY_EDITOR
			new AndroidVibrator();
#else
			new UnityVibrator();
#endif

		public static void Vibrate() => Vibrator.Vibrate();


		public static void Vibrate(long milliseconds) => Vibrator.Vibrate(milliseconds);

		public static void Vibrate(long[] pattern, int repeat) => Vibrator.Vibrate(pattern, repeat);

		public static bool HasVibrator() => Vibrator.GetType() != typeof(UnityVibrator);

		public static void Cancel() => Vibrator.Cancel();
	}
}