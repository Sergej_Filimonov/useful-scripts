using UnityEngine;

namespace Helpers.Vibration
{
	public sealed class AndroidVibrator : IVibrator
	{
		private static readonly AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		private static readonly AndroidJavaObject CurrentActivity =
			UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		private static readonly AndroidJavaObject Vibrator =
			CurrentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");

		public void Vibrate() => Vibrator.Call("vibrate");
		public void Vibrate(long milliseconds) => Vibrator.Call("vibrate", milliseconds);
		public void Vibrate(long[] pattern, int repeat) => Vibrator.Call("vibrate", pattern, repeat);
		public void Cancel() => Vibrator.Call("cancel");
	}
}