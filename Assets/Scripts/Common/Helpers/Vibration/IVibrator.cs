namespace Helpers.Vibration
{
	public interface IVibrator
	{
		void Vibrate();
		void Vibrate(long milliseconds);
		void Vibrate(long[] pattern, int repeat);
		void Cancel();
	}
}