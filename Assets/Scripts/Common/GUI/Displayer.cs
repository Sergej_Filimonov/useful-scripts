﻿using System;
using Common.Container;

namespace Common.GUI
{
    public sealed class Displayer : IDisplayer
    {
        public event Action<object, IViewerTarget, Type> DisplayTargetViewer = delegate { };
        public event Action<Type> DisplayViewer = delegate { };
        public event Action<Type> Hided = delegate { };
        public event Action<object> TargetHided = delegate { };

        void IDisplayer.Display<T>(T target)
        {
            DisplayTargetViewer(target, null, typeof(T));
        }

        void IDisplayer.DisplayViewerWithTarget<T>(IViewerTarget target)
        {
            DisplayTargetViewer(null, target, typeof(T));
        }

        void IDisplayer.Display(Type viewerType)
        {
            DisplayViewer(viewerType);
        }
        
        void IDisplayer.Hide(Type viewerType)
        {
            Hided(viewerType);
        }
        
        void IDisplayer.Hide(object target)
        {
            TargetHided(target);
        }

        Type IContainerItem.ItemType
        {
            get { return typeof(IDisplayer); }
        }

        void IDisposable.Dispose()
        {
            DisplayTargetViewer = delegate { };
            DisplayViewer = delegate { };
            Hided = delegate { };
            TargetHided = delegate { };
        }
    }
}
