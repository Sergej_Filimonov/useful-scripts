﻿using System;
using Common.Container;
using UnityEngine;

namespace Common.GUI
{
	public interface IDialogManager : IContainerItem
	{
		event Action OnDialogClosedEvent;

		void CloseAllOpenedDialogs();
		T GetDialog<T>() where T : Component, IDialog;
		T SpawnDialogPrefab<T>() where T : Component, IDialog;
		void Close(GameObject dialog);
	}
}