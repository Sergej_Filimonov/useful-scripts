﻿using UnityEngine;

namespace Common.GUI
{
    public abstract class Viewer : MonoBehaviour
    {
        [SerializeField] protected GameObject objectToHide = null;
        [SerializeField] protected bool hideOnAwake = true;

        public bool IsShown => objectToHide.activeSelf;

        protected virtual void Awake()
        {
            CheckObjectToHide();
//            objectToHide.SetActive(!hideOnAwake); // TODO: this caused more harm than good
        }

        protected virtual void OnDestroy()
        {
        }

        protected void CheckObjectToHide()
        {
            if (objectToHide == null)
                objectToHide = gameObject;
        }
    }
}
