﻿namespace Common.GUI
{
    public abstract class BaseViewer : Viewer, IViewer
    {   
        bool IViewer.HideOnAwake
        {
            get { return hideOnAwake; }
        }

        public virtual void Show()
        {
            CheckObjectToHide();
            if (objectToHide != null)
                objectToHide.SetActive(true);
        }

        public virtual void Hide()
        {
            CheckObjectToHide();
            if (objectToHide != null)
                objectToHide.SetActive(false);
        }
    }
}
