﻿using System;

namespace Common.GUI
{
    public abstract class BaseTargetViewer<TTarget> : Viewer, ITargetViewer 
    {
        protected TTarget Target { get; set; }           

        Type ITargetViewer.TargetType => typeof (TTarget);

        bool ITargetViewer.HideOnAwake => hideOnAwake;

        public bool IsHidden
        {
	        get
	        {
		        if (objectToHide == null) return true;
		        else return !objectToHide.activeSelf;
	        }
        }

        void ITargetViewer.Show(object target) { Show((TTarget)target); }

        public virtual void Show(TTarget target)
        {
            Target = target;
            if (objectToHide != null)
                objectToHide.SetActive(true);
        }

        public virtual void Hide()
        {
            Target = default(TTarget);
            if (objectToHide != null)
                objectToHide.SetActive(false);
        }
    }
}
