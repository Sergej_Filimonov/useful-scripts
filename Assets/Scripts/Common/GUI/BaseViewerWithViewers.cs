﻿using System.Collections.Generic;
using Common.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace Common.GUI
{
    public abstract class BaseViewerWithViewers<TViewerPrefab> : BaseViewer
        where TViewerPrefab : MonoBehaviour
    {
        [SerializeField] private TViewerPrefab viewerPrefab = null;
        [SerializeField] private GridLayoutGroup grid = null;
        protected readonly List<TViewerPrefab> viewers = new List<TViewerPrefab>();

        protected override void Awake()
        {
            base.Awake();
            this.Check("cueViewerPrefab", viewerPrefab != null, LogType.Error);
            this.Check("grid", grid != null, LogType.Error);
        }

        protected void CreateViewers(int count)
        {
            if (count <= 0 || viewerPrefab == null) return;

            for (int i = 0; i < count; i++)
            {
                var v = Instantiate(viewerPrefab) as TViewerPrefab;
                v.transform.SetParent(grid.transform);
                v.transform.ResetLocal();
                viewers.Add(v);
            }
        }
    }
}
