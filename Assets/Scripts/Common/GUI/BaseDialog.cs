﻿using Common.Container;
using UnityEngine;

namespace Common.GUI
{
	public abstract class BaseDialog : MonoBehaviour, IDialog
	{
		[SerializeField] protected bool forceCurrentCanvasOrder;
		public bool ForceCurrentCanvasOrder => forceCurrentCanvasOrder;

		public Canvas Canvas { get; private set; }

		protected virtual void Awake()
		{
			Canvas = GetComponent<Canvas>();
		}

		public void Close()
		{
//			ContainerManager.Get<IDialogManager>().Close(gameObject);
		}
	}
}