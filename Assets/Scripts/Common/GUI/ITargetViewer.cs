﻿using System;

namespace Common.GUI
{
    public interface ITargetViewer
    {       
        Type TargetType { get; }

        bool HideOnAwake { get; }

        void Show(object target);

        void Hide();
    }
}
