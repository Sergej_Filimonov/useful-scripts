﻿using System;
using Common.Container;

namespace Common.GUI
{
    public interface IDisplayer : IContainerItem
    {
        event Action<object, IViewerTarget, Type> DisplayTargetViewer;
        event Action<Type> DisplayViewer;
        event Action<Type> Hided;
        event Action<object> TargetHided;

        void Display<T>(T target);

        void DisplayViewerWithTarget<T>(IViewerTarget target);

        void Display(Type viewerType);

        void Hide(Type viewerType);

        void Hide(object target);
    }
}
