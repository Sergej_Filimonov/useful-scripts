﻿using System;

namespace Common.GUI
{
    public interface IViewerTarget
    {
        Type TargetType { get; }

        object Target { get; }
    }
}
