﻿namespace Common.GUI
{
    public interface IViewer
    {
        bool HideOnAwake { get; }

        void Show();

        void Hide();
    }
}
