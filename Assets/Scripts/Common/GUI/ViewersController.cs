﻿//using System;
//using System.Collections.Generic;
//using Common.Container;
//using Common.Extensions;
//using UnityEngine;
//
//namespace Common.GUI
//{
//    public sealed class ViewersController : MonoBehaviour
//    {
//        private readonly Dictionary<Type, ITargetViewer> targetViewers = new Dictionary<Type, ITargetViewer>();
//        private readonly Dictionary<Type, IViewer> simlpeViewers = new Dictionary<Type, IViewer>();
//
//        public void Init()
//        {
//            if (ContainerManager.Contains<IDisplayer>())
//            {
//                ContainerManager.Get<IDisplayer>().Dispose();
//                ContainerManager.Remove<IDisplayer>();
//            }
//            ContainerManager.Add<IDisplayer>(new Displayer());
//            foreach (var v in gameObject.GetGenericComponentsInChildren<ITargetViewer>())
//            {
//                if (!targetViewers.ContainsKey(v.TargetType))
//                    targetViewers.Add(v.TargetType, v);
//                if (v.HideOnAwake)
//                    v.Hide();
//            }
//
//            foreach (var v in gameObject.GetGenericComponentsInChildren<IViewer>())
//            {
//                if (!simlpeViewers.ContainsKey(v.GetType()) && !targetViewers.ContainsKey(v.GetType()))
//                    simlpeViewers.Add(v.GetType(), v);
//                if (v.HideOnAwake) v.Hide();
//                else v.Show();
//            }
//
//            ContainerManager.Get<IDisplayer>().DisplayTargetViewer += OnDisplayTargetViewer;
//            ContainerManager.Get<IDisplayer>().Hided += OnSomethingNeedToHide;
//            ContainerManager.Get<IDisplayer>().DisplayViewer += OnDisplaySimpleViewer;
//            ContainerManager.Get<IDisplayer>().TargetHided += OnTargetHided;
//        }
//
//        private void OnDestroy()
//        {
//            if (!ContainerManager.Contains<IDisplayer>()) return;
//            ContainerManager.Get<IDisplayer>().DisplayTargetViewer -= OnDisplayTargetViewer;
//            ContainerManager.Get<IDisplayer>().Hided -= OnSomethingNeedToHide;
//            ContainerManager.Get<IDisplayer>().DisplayViewer -= OnDisplaySimpleViewer;
//            ContainerManager.Get<IDisplayer>().TargetHided -= OnTargetHided;
//            ContainerManager.Get<IDisplayer>().Dispose();
//            ContainerManager.Remove<IDisplayer>();
//        }
//
//        private void OnDisplayTargetViewer(object obj, IViewerTarget viewerTarget, Type type)
//        {
//            ITargetViewer v;
//            var target = obj;
//			if (viewerTarget != null) {
//				type = viewerTarget.TargetType;
//				target = viewerTarget.Target;
//			} 
//            if (type != null && targetViewers.TryGetValue(type, out v))
//			{
//                v.Show(target);
//            }
//        }
//
//        private void OnDisplaySimpleViewer(Type type)
//        {
//            IViewer v = null;
//            if (simlpeViewers.TryGetValue(type, out v))
//                v.Show();
//        }
//
//        private void OnSomethingNeedToHide(Type t)
//        {
//            ITargetViewer v = null;
//            if (targetViewers.TryGetValue(t, out v))
//            {
//                v.Hide();
//            }
//            else
//            {
//                IViewer v1 = null;
//                if (simlpeViewers.TryGetValue(t, out v1))
//                    v1.Hide();
//            }
//        }
//
//        private void OnTargetHided(object obj)
//        {
//            ITargetViewer v = null;
//            Type type = null;
//
//            if (obj != null)
//                type = obj.GetType();
//
//            if (type != null && targetViewers.TryGetValue(type, out v))
//            {
//                v.Hide();
//            }
//        }
//    }
//}
