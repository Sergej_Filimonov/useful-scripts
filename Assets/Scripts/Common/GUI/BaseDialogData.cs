﻿using Common.Container;
using UnityEngine;

namespace Common.GUI
{
	public abstract class BaseDialogData<T> : MonoBehaviour, IDialog
	{
		[SerializeField] protected bool forceCurrentCanvasOrder;
		public bool ForceCurrentCanvasOrder => forceCurrentCanvasOrder;
		public T Data { get; private set; }
		public Canvas Canvas { get; private set; }

		protected virtual void Awake()
		{
			Canvas = GetComponent<Canvas>();
		}

		public void Initialize(T data)
		{
			Data = data;
			OnInitialize();
		}

		protected virtual void OnInitialize() {}

		protected virtual void OnClose() {}

		public virtual void Close()
		{
			OnClose();
//			ContainerManager.Get<IDialogManager>().Close(gameObject);
		}
	}

	public interface IDialog
	{
		Canvas Canvas { get; }
		bool ForceCurrentCanvasOrder { get; }
		void Close();
	}
}