﻿using System;
using Common.SceneManagement;
using UnityEngine;

namespace Common.Architecture.Initialization
{
	public abstract class BaseInitializer : MonoBehaviour
	{
		private void Awake()
		{
			if (IsApplicationNotInitialized())
			{
				ScenesLoader.LoadScene(ApplicationScene.Application, true, false, false);
				return;
			}

			Debug.Log($"Initializing {gameObject.name}...");
			Init();
		}

		private void Start() => PostInit();

		private void OnDestroy()
		{
			Debug.Log($"Deinitializing {gameObject.name}...");
			Deinit();
		}

		protected abstract void Init();
		protected abstract void PostInit();
		protected abstract void Deinit();

		private static bool IsApplicationNotInitialized() => !ApplicationManager.IsInitialized &&
			!ScenesLoader.CurrentSceneName.Equals(ApplicationScene.Application.ToString(),
				StringComparison.OrdinalIgnoreCase);
	}
}