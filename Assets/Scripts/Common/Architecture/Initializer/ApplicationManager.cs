﻿using UnityEngine;

namespace Common.Architecture
{
    public class ApplicationManager : MonoBehaviour
    {
        public static ApplicationManager Instance { get; internal set; }
        public static bool IsInitialized { get; internal set; } = false;

        private void Awake()
        {
            Instance = this;
            IsInitialized = true;
        }
    }
}