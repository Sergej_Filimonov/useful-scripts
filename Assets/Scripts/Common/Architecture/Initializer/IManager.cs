namespace Common.Architecture.Initialization
{
	public interface IManager
	{
		void Start();
		void Stop();
	}
}