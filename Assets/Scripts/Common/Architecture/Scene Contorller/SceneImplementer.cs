﻿using Common.SceneManagement;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common.Architecture
{
    public class SceneImplementer<ControllerType, SceneDataType, DType> 
        where ControllerType : SceneController<SceneDataType>, new ()
        where SceneDataType : class
    {
        #region PublicStaticProperty
        public static ApplicationScene Scene { get; protected set; }
        #endregion

        #region Events
        public event Action Loaded;
        #endregion

        #region PublicProperty
        public bool IsLoaded { get; protected set; }
        public ControllerType Controller { get; protected set; }
        #endregion

        #region PrivetFields
        protected DType data;
        protected ILoadingScreenHolder loadingScreenHolder;
        #endregion

        #region Initialization
        public SceneImplementer()
        {
            SetScene();
            IsLoaded = false;
        }

        public SceneImplementer(DType data) : this()
        {
            this.data = data;
        }

        protected virtual void SetScene() { }
        #endregion

        #region PublicMethods
        public void Load(bool setActive = true, bool loadAsync = true, ILoadingScreenHolder loadingScreenHolder = null)
        {
            if (loadingScreenHolder != null) this.loadingScreenHolder = loadingScreenHolder;

            ScenesLoader.SceneLoaded += OnSceneLoaded;
            ScenesLoader.LoadScene(Scene, setActive, loadAsync, loadingScreenHolder: loadingScreenHolder);
        }

        public void Unload()
        {
            IsLoaded = false;
            Controller.Stop();
            Controller = null;
            ScenesLoader.UnloadScene(Scene);
        }
        #endregion

        #region PrivetMethods
        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            if (!string.Equals(scene.name, Scene.ToString())) return;
            ScenesLoader.SceneLoaded -= OnSceneLoaded;
            Controller = new ControllerType();
            var sceneDataType = typeof(SceneDataType);
            var sceneData = GameObject.FindObjectOfType(sceneDataType);
            if (sceneData != null) Controller.SetupData(sceneData as SceneDataType);
            Controller.Start();
            IsLoaded = true;
            Loaded?.Invoke();
            Loaded = null;
            if (loadingScreenHolder != null)
                loadingScreenHolder.HideLoadingScreen();
        }
        #endregion
    }
}