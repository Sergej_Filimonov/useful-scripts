﻿namespace Common.Architecture
{
    public class SceneController<SceneDataType>
    {
        #region PrivetFields
        protected SceneDataType sceneData;
        #endregion

        #region Initialization
        public SceneController()
        {
            Init();
        }

        public void SetupData(SceneDataType gameSceneData)
        {
            sceneData = gameSceneData;
            OnDataSetup();
        }

        protected virtual void Init() { }
        protected virtual void OnDataSetup() { }
        #endregion

        #region IController
        public virtual void Start()
        {

        }

        public virtual void Stop()
        {

        }
        #endregion
    }
}