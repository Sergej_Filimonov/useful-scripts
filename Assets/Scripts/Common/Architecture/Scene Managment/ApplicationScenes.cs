﻿namespace Common.SceneManagement
{
    public enum ApplicationScene
    {
        EntryPoint,
        LoadingScreen,
        UICamera,
        Application,
        AudioSystem,
        MainMenu,
        Game,
        GameField,
        DebugReporter
    }
}