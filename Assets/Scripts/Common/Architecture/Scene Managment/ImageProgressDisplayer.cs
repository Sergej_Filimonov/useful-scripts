﻿using UnityEngine;
using UnityEngine.UI;

namespace Common.SceneManagement
{
    [RequireComponent(typeof(Image))]
    public class ImageProgressDisplayer : MonoBehaviour, IProgressDiplayer
    {
        private Image image;

        public float CurrentProgress => image.fillAmount;

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        public void SetProgress(float value)
        {
            image.fillAmount = Mathf.Clamp01(value);
        }
    }
}