﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.SceneManagement
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private MonoBehaviour parent = null;
        [SerializeField] private GameObject progressDispalyerObject = null;
        [SerializeField] private float progressLerpTime = 5;

        private IProgressDiplayer progressDiplayer;
        private List<AsyncOperation> targets = new List<AsyncOperation>();
        private float averageProgress = 0;
        private bool allTargetsIsDone = true;

        private void Awake()
        {
            progressDiplayer = progressDispalyerObject.GetComponent<IProgressDiplayer>();

            if (progressDiplayer == null)
            {
                Debug.LogError("Progress displayer is NULL!");
            }

            gameObject.SetActive(false);
        }

        public void Show(AsyncOperation asyncOperation)
        {
            targets.Add(asyncOperation);
            allTargetsIsDone = false;

            if (gameObject.activeSelf)
            {
                return;
            }

            gameObject.SetActive(true);
            progressDiplayer.SetProgress(0);
            parent.StartCoroutine(AsyncProgress());
        }

        public void Hide()
        {
            if (!allTargetsIsDone) return;
            gameObject.SetActive(false);
            parent.StopAllCoroutines();
            targets.Clear();
        }

        private IEnumerator AsyncProgress()
        {
            while (!allTargetsIsDone)
            {
                averageProgress = 0;
                allTargetsIsDone = true;
                foreach (var item in targets)
                {
                    averageProgress += item.progress;
                    if (!item.isDone)
                    {
                        allTargetsIsDone = false;
                    }
                }
                averageProgress = averageProgress / targets.Count;

                progressDiplayer.SetProgress(Mathf.Lerp(progressDiplayer.CurrentProgress, averageProgress, Time.deltaTime * progressLerpTime));

                yield return null;
            }

            yield return new WaitForSecondsRealtime(1.0f);
            Hide();
        }
    }
}