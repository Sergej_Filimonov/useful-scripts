﻿using System.Collections.Generic;
using Common.Modifiers;

namespace Common.AttributeParameters
{
    public sealed class AttributeParameter : IAttributeParameter
    {
        private readonly IList<IModifier> modifiers = new List<IModifier>();

        public float Value { get; private set; }

        public float BaseValue { get; private set; }

        public AttributeParameter(float baseValue)
        {
            BaseValue = baseValue;
            Value = baseValue;
        }
        
        public void ApplyModifier(IModifier modifier)
        {
            modifiers.Add(modifier);
            CalculateValue();
        }

        public void RemoveModifier(IModifier modifier)
        {
            modifiers.Remove(modifier);
            CalculateValue();
        }

        public void RemoveAll()
        {
            modifiers.Clear();
            CalculateValue();
        }

        private void CalculateValue()
        {
            Value = BaseValue;
            for (int i = 0; i < modifiers.Count; i++)
            {
                if (modifiers[i].Type == ModifierType.Absolute)
                    Value += modifiers[i].Value;
                else
                    Value += modifiers[i].Value * BaseValue;
            }
            if (Value < 0.0f)
                Value = 0.0f;
        }
    }
}