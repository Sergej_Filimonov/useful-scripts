﻿using Common.Modifiers;

namespace Common.AttributeParameters
{
    public interface IAttributeParameter
    {
        float Value { get; }

        float BaseValue { get; }

        void ApplyModifier(IModifier modifier);

        void RemoveModifier(IModifier modifier);

        void RemoveAll();
    }
}
