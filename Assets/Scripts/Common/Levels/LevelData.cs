﻿using System;
using UnityEngine;

namespace Common.Levels
{
    [CreateAssetMenu(fileName = "Level", menuName = "Configs/Level")]
    public class LevelData : ScriptableObject
    {
        public string LevelName = "Level";
        public DifficultyLevel Difficulty = DifficultyLevel.Easy;
    }

    [Serializable]
    public enum DifficultyLevel
    {
        Easy = 0,
        Normal = 1,
        Hard = 2,
        Expert = 3
    }
}