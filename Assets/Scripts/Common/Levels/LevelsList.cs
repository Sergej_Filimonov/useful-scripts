﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.Levels
{
    [CreateAssetMenu(fileName = "Levels List", menuName = "Configs/Levels List")]
    public class LevelsList : ScriptableObject
    {
        [SerializeField] private List<LevelData> levels = null;

        public List<LevelData> Levels => levels;

        public bool HasLevelAtIndex(int index) => index >= 0 && index < Levels.Count;
    }
}