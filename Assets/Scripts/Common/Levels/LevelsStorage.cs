﻿using Common.Serialization;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Levels
{
    public static class LevelsStorage
    {
        private const string LEVELS_KEY = "Levels";
        private const string LEVELSRESULTS_KEY = "LevelsResults";

        private static HashSet<int> levels = new HashSet<int>();
        private static Dictionary<int, LevelResult> levelsResults = new Dictionary<int, LevelResult>();

        public static void Load()
        {
            if (PlayerPrefs.HasKey(LEVELS_KEY))
                levels = (HashSet<int>)StringSerializationAPI.Deserialize(typeof(HashSet<int>), PlayerPrefs.GetString(LEVELS_KEY));

            if (PlayerPrefs.HasKey(LEVELSRESULTS_KEY))
                levelsResults = (Dictionary<int, LevelResult>)StringSerializationAPI.Deserialize(typeof(Dictionary<int, LevelResult>), PlayerPrefs.GetString(LEVELSRESULTS_KEY));

            var emptyList = new List<int>();

            foreach (var item in levelsResults)
            {
                if (item.Value == null)
                {
                    emptyList.Add(item.Key);
                }
            }

            foreach (var item in emptyList)
            {
                Debug.LogWarning("empty levelsResults");
                levelsResults[item] = new LevelResult();
            }
        }

        public static void Save()
        {
            PlayerPrefs.SetString(LEVELS_KEY, StringSerializationAPI.Serialize(typeof(HashSet<int>), levels));
            PlayerPrefs.SetString(LEVELSRESULTS_KEY, StringSerializationAPI.Serialize(typeof(Dictionary<int, LevelResult>), levelsResults));
        }

        public static void SeveLevelResult(int levelIndex, LevelResult result)
        {
            if (result == null)
            {
                return;
            }

            if (levelsResults.ContainsKey(levelIndex))
            {
                if (levelsResults[levelIndex].Score < result.Score)
                {
                    levelsResults[levelIndex] = result;
                }
            }
            else
            {
                levelsResults.Add(levelIndex, result);
            }

            Save();
        }

        public static bool GetLevelResult(int levelIndex, out LevelResult levelResult)
        {
            levelResult = new LevelResult();
            bool result = levelsResults.ContainsKey(levelIndex);

            if (result)
            {
                levelResult = levelsResults[levelIndex];
            }

            return result;
        }

        public static void AddNewLevel(int index)
        {
            levels.Add(index);

            Save();
        }

        public static bool HasLevel(int index) => levels.Contains(index);

        public static void RemoveLevel(int index)
        {
            if (levels.Contains(index))
            {
                levels.Remove(index);
            }
        }
    }
}