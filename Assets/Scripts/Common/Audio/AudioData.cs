﻿using System;
using UnityEngine;

namespace Common.Audio
{
    [Serializable]
    public class AudioData
    {
        public AudioTypes Type;
        public AudioClip AudioClip;
        public float Volume;
    }
}