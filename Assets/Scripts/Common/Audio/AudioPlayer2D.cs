﻿using DG.Tweening;
using System;
using UniRx;
using UnityEngine;

namespace Common.Audio
{
    public sealed class AudioPlayer2D : IDisposable
    {
        public event Action StartPlaying = delegate { };
        public event Action ContinuePlaying = delegate { };
        public event Action SetOnPause = delegate { };
        public event Action TimingChanged = delegate { };
        public event Action ClipChanged = delegate { };
        public event Action<AudioSource> Disposed;

        private readonly AudioSource audioSource;
        private readonly IDisposable updater;
        private readonly float originalVolume;

        private const float FADE_IN_TIME = 1.0f;
        private const float FADE_OUT_TIME = 1.0f;

        public bool IsPlaying => audioSource.isPlaying;
        public float CurrentTime => audioSource.clip == null ? 0f : audioSource.time;
        public float ClipDuration => audioSource.clip == null ? 0f : audioSource.clip.length;
        public AudioClip CurrentClip { get; private set; }
        public string DisplayName { get => CurrentClip == null ? "" : CurrentClip.name; }
        public void Mute(bool value = true) => audioSource.mute = value;
        public void SetLoop(bool value) => audioSource.loop = value;

        public AudioPlayer2D(AudioSource audioSource)
        {
            this.audioSource = audioSource;
            originalVolume = this.audioSource.volume;
            updater = Observable.EveryGameObjectUpdate().Subscribe(DoUpdate);
        }

        public void Stop() => audioSource.Stop();

        public void Play() => Play(FADE_IN_TIME);

        public void Play(float fadeTime)
        {
            audioSource.Play();
            audioSource.DOKill();
            audioSource.volume = 0;
            audioSource.DOFade(originalVolume, fadeTime);
            StartPlaying();
        }

        public void Pause() => Pause(FADE_OUT_TIME);

        public void Pause(float fadeTime)
        {
            audioSource.DOKill();
            audioSource.DOFade(0, fadeTime).OnComplete(audioSource.Pause);
            SetOnPause();
        }

        public void UnPause() => UnPause(FADE_IN_TIME);

        public void UnPause(float fadeTime)
        {
            audioSource.UnPause();
            audioSource.DOKill();
            audioSource.volume = 0;
            audioSource.DOFade(originalVolume, fadeTime);
            ContinuePlaying();
        }

        public void PlayImmediate()
        {
            audioSource.DOKill();
            audioSource.Play();
            audioSource.volume = originalVolume;
            StartPlaying();
        }

        public void PauseImmediate()
        {
            audioSource.DOKill();
            audioSource.Pause();
            SetOnPause();
        }

        public void UnPauseImmediate()
        {
            audioSource.DOKill();
            audioSource.UnPause();
            audioSource.volume = originalVolume;
            ContinuePlaying();
        }

        public void SetAudioClip(AudioClip audioClip)
        {
            audioSource.Stop();
            audioSource.time = 0.0f;
            CurrentClip = audioClip;
            audioSource.clip = CurrentClip;
            ClipChanged();
        }

        public void SetTime(float time)
        {
            if (time == audioSource.time || time > ClipDuration)
            {
                Debug.LogError("Invalid time!");
                return;
            }
            audioSource.time = time;
        }

        private void DoUpdate(long frameRate)
        {
            if (audioSource.clip != null) TimingChanged();
        }

        public void Dispose() 
        {
            updater?.Dispose();
            audioSource.Stop();
            audioSource.volume = originalVolume;
            audioSource.clip = null;
            audioSource.loop = false;
            Disposed?.Invoke(audioSource);
            Disposed = null;
            StartPlaying = null;
            ContinuePlaying = null;
            SetOnPause = null;
            ClipChanged = null;
            TimingChanged = null;
        }
    }
}