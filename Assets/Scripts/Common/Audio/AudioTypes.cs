﻿using System;

namespace Common.Audio
{
    [Serializable]
    public enum AudioTypes
    {
        SoundFX,
        Music,
        UISound,
        Voice,
        Ambient,
        Global
    }
}