﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.Audio
{
    public class AudioSystem2D : MonoBehaviour
    {
        [SerializeField] private List<AudioSource> audioSources = new List<AudioSource>();

        private const int MINIMUM_AUDIO_SOURCES_COUNT = 5;

        private static AudioSystem2D instance;

        private int cachedIndex = 0;
        private Dictionary<AudioClip, AudioSource> cachedSources = new Dictionary<AudioClip, AudioSource>();

        private void Awake()
        {
            instance = this;

            if (audioSources.Count == 0)
            {
                for (int i = 0; i < MINIMUM_AUDIO_SOURCES_COUNT; i++)
                {
                    audioSources.Add(CreateAudioSource());
                }
            }
        }

        public static AudioPlayer2D GetAudioPlayer2D()
        {
            var audioSource = GetFreeAudioSource();
            instance.audioSources.Remove(audioSource);
            var player = new AudioPlayer2D(audioSource);
            player.Disposed += audioS => AddAudioSource(audioS);
            return player;
        }

        public static AudioSource ReservationAudioSource()
        {
            var audioSource = GetFreeAudioSource();
            instance.audioSources.Remove(audioSource);
            return audioSource;
        }

        public static void AddAudioSource(AudioSource audioSource) => instance.audioSources.Add(audioSource);

        public static void PlayOneShot(AudioData audioData)
        {
            PlayOneShot(audioData.AudioClip, audioData.Volume);
        }

        public static void PlayOneShot(AudioClip audioClip, float volume = 1)
        {
            var audioSource = GetFreeAudioSource();
            audioSource.volume = volume;
            audioSource.PlayOneShot(audioClip);
        }

        public static void Play(AudioData audioData)
        {
            Play(audioData.AudioClip, audioData.Volume);
        }

        public static void Play(AudioClip audioClip, float volume = 1)
        {
            var audioSource = GetFreeAudioSource();
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.Play();
            CachedAudioClip(audioClip, audioSource);
        }

        public static void Stop(AudioClip audioClip)
        {
            if (GetAudioSourceWithClip(audioClip, out AudioSource audioSource))
            {
                audioSource.Stop();
            }
        }

        public static void PlayLoop(AudioData audioData)
        {
            PlayLoop(audioData.AudioClip, audioData.Volume);
        }

        public static void PlayLoop(AudioClip audioClip, float volume = 1)
        {
            var audioSource = GetFreeAudioSource();
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.loop = true;
            audioSource.Play();
            CachedAudioClip(audioClip, audioSource);
        }

        public static void StopLoop(AudioClip audioClip)
        {
            if (GetAudioSourceWithClip(audioClip, out AudioSource audioSource))
            {
                audioSource.loop = false;
                audioSource.Stop();
            }
        }

        private static void CachedAudioClip(AudioClip audioClip, AudioSource audioSource)
        {
            if (instance.cachedSources.ContainsKey(audioClip))
            {
                instance.cachedSources[audioClip] = audioSource;
            }
            else
            {
                instance.cachedSources.Add(audioClip, audioSource);
            }
        }

        private static AudioSource GetFreeAudioSource()
        {
            for (int i = instance.cachedIndex; i < instance.audioSources.Count; i++)
            {
                if (!instance.audioSources[i].isPlaying)
                {
                    instance.cachedIndex = i + 1;
                    return instance.audioSources[i];
                }
            }

            for (int i = 0; i < instance.cachedIndex; i++)
            {
                if (!instance.audioSources[i].isPlaying)
                {
                    instance.cachedIndex = i + 1;
                    return instance.audioSources[i];
                }
            }

            return CreateAudioSource();
        }

        private static bool GetAudioSourceWithClip(AudioClip audioClip, out AudioSource audioSource)
        {
            audioSource = null;
            bool result = false;

            if (instance.cachedSources.ContainsKey(audioClip))
            {
                audioSource = instance.cachedSources[audioClip];
                result = true;
            }

            return result;
        }

        private static AudioSource CreateAudioSource()
        {
            GameObject go = new GameObject("Audio Source 2D");
            go.transform.parent = instance.transform;
            var audioSource = go.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            return audioSource;
        }
    }
}