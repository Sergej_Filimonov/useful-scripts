﻿using Common.Serialization;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Audio
{
    public class AudioSettings
    {
        private const string KEY = "Audio Settings";

        private Dictionary<AudioTypes, AudioSettingsData> data;
        public AudioSettings()
        {
            if (!Load())
            {
                data = new Dictionary<AudioTypes, AudioSettingsData>();
                foreach (var item in Enum.GetValues(typeof(AudioTypes)))
                {
                    AudioSettingsData audioSettingsData = new AudioSettingsData();
                    audioSettingsData.Type = (AudioTypes)item;
                    data.Add(audioSettingsData.Type, audioSettingsData);
                }
            }
        }

        public AudioSettingsData GetDataByType(AudioTypes type)
        {
            return data[type];
        }

        public void Save()
        {
            string dataToSave = StringSerializationAPI.Serialize(typeof(Dictionary<AudioTypes, AudioSettingsData>), data);
            PlayerPrefs.SetString(KEY, dataToSave);
        }

        private bool Load()
        {
            bool result = false;
            if (PlayerPrefs.HasKey(KEY))
            {
                data = (Dictionary<AudioTypes, AudioSettingsData>)StringSerializationAPI.Deserialize(typeof(Dictionary<AudioTypes, AudioSettingsData>), PlayerPrefs.GetString(KEY));
                result = true;
            }
            return result;
        }
    }
}