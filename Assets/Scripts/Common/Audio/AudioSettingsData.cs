﻿using System;
using UnityEngine;

namespace Common.Audio
{
    [Serializable]
    public class AudioSettingsData
    {
        private const int MAX_VALUE = 0;
        private const int MIN_VALUE = -80;

        public AudioTypes Type = AudioTypes.SoundFX;
        public bool IsActive = true;
        public float VolumeValue = 1.0f;

        public int GetVolumeOnDb()
        {
            return (int)Mathf.Lerp(MIN_VALUE, MAX_VALUE, VolumeValue);
        }
    }
}