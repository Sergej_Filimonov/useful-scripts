﻿namespace Common.Modifiers
{
    public interface IModifier
    {
        float Value { get; }

        ModifierType Type { get; }
    }
}
