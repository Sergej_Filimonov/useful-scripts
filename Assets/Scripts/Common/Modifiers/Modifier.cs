﻿namespace Common.Modifiers
{
    public struct Modifier : IModifier
    {
        private readonly float modifierValue;
        private readonly ModifierType modifierType;

        public float Value
        {
            get { return modifierValue; }
        }

        public ModifierType Type
        {
            get { return modifierType; }
        }

        public Modifier(ModifierType type, float value)
        {
            modifierType = type;
            modifierValue = value;
        }
    }
}
