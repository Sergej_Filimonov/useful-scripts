﻿namespace Common.Modifiers
{
    public enum ModifierType
    {
        Absolute,
        Relative
    }
}
